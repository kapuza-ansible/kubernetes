# kubernetes
Look examples directory.

* kubectl_install.yml - install only kubectl
* kubernetes_install - install kubernetes

## After install
```bash
# Initialize Kubernetes (master node)
kubeadm init --pod-network-cidr=10.244.0.0/16

# Set connect to kubernetes
mkdir -p $HOME/.kube; \
cp -i /etc/kubernetes/admin.conf $HOME/.kube/config; \
chown $(id -u):$(id -g) $HOME/.kube/config

# Add kubectl completions
echo 'source <(kubectl completion bash)' >>~/.bashrc; \
source <(kubectl completion bash)

# Wate while all pods is Running (not Pending) - ignore coredns
kubectl get pods --all-namespaces

# Deploy a Pod Network through (master node)
kubectl apply \
-f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml

# Get join info from master
kubeadm token create --print-join-command

# Join (worker nodes)
kubeadm join ...
```

## On master node
```bash
# Wate when ready - Ready
kubectl get nodes -o wide

# Get pods
kubectl get pods --all-namespaces -o wide

# Get cluster info
kubectl cluster-info
```

